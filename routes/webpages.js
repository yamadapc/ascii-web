'use strict';
var child_process = require('child_process');
var Promise = require('bluebird');
var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
  var url = req.query.url;
  if(!url) {
    var err = new Error('Parameter `url` is required');
    err.status = 400;
    throw err;
  }

  res.json({
    webpage: fetchAndRender(url),
  });
});

function fetchAndRender(url) {
  return new Promise(function(fulfill, reject) {
    var output = '';
    var p = child_process
      .spawn('w3m', ['-dump', url]);

    p.stdout.on('data', function(d) {
      output += d;
    });

    p.on('close', function(code) {
      if(code !== 0) {
        return reject(new Error('Failed fetching your webpage :('));
      }

      fulfill(output);
    });
  });
}

module.exports = exports = router;
