'use strict';
var bodyParser = require('body-parser');
var cors = require('express-cors');
var express = require('express');
var expressPromise = require('express-promise');
var logger = require('morgan');

var webpages = require('./routes/webpages');

var app = express();

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors({
  allowedOrigins: [ '*' ],
}));
app.use(expressPromise());

app.use('/api/webpages', webpages);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

module.exports = app;

// jshint ignore:start
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err
    });
  });
}

app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    message: err.message,
  });
});
// jshint ignore:end
